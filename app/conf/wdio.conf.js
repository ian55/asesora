exports.config = {
  specs: [
    './test/e2e/**/*.js'
  ],
  maxInstances: 1,
  host: 'selenium',
  port: 4444,
  baseUrl: 'http://app:8080',
  capabilities: [{
    browserName: 'chrome'
  }],
  reporters: ['spec'],
  framework: 'mocha',
  mochaOpts: {
    timeout: 40000
  }
}
