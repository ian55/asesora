import { APIClient } from '../infrastructure/api_client';
import BuildCallback from '../infrastructure/buildCallback';
import { Bus } from '../bus';

export default class Service {
  constructor() {
    this.client = APIClient;
    this.buildCallback = BuildCallback;
    this.bus = Bus;
    this.subscribe();
  }

  subscribe(){}

}
