
class MaxFileSize {
  static isBelow(file) {
    const maxFileSize = 52428800

    return (file.size <= maxFileSize && file.size > 0)
  }
}

export default MaxFileSize
