import Model from './model'

export default class CatalogsManager extends Model {
  constructor() {
    super()
    this.labels = {
      title: "",
      source: "",
      ccaa: "",
      organizations: "",
      topics: "",
      proposals: "",
      reasons: "",
      projects: "",
      cnae: "",
      addItem: "",
      save: "",
      notValidID: "",
      id: "",
      text: "",
      itemAdded: ""
    }
    this.values = {
      "catalog": "",
      "catalogsList": [],
      "catalogName" : "",
      "newItemValue": "",
      "newItemID": "",
      "selectedTitle": ""
    }
    this.sourceCatalog = []
    this.ccaaCatalog = []
    this.organizationsCatalog = []
    this.topicsCatalog = []
    this.proposalsCatalog = []
    this.reasonsCatalog = []
    this.projectsCatalog = []
    this.cnaeCatalog =[]
    this.showInput = false
    this.validId = false
  }
}
