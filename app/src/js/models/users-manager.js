import Model from './model'

export default class UserManager extends Model {
  constructor() {
    super()
    this.labels = {
      title: "",
      email: "",
      authorizedEmails: "",
      errorEmail: "",
      emailAdd: "",
      remove: "",
      removeConfirmMessage: ""
    }
    this.values = {
      emails: [""],
      emailNew: "",
      coordinatorEmails: [""],
      coordinatorEmailNew: ""
    }
    this.control = {
      isValidEmail: false,
      hasError: false,
      isValidCoordinatorEmail: false,
      hasCoordinatorError: false
    }
  }
}
