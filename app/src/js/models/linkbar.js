import Model from './model'

export default class LinkBar extends Model {
  constructor() {
    super()
    this.labels = {
      "feedbackForm": ""
    }
    this.values = {
      "feedbackFormLink": "https://docs.google.com/forms/d/e/1FAIpQLSfLE2MWvaN1HjeoWNADqOwpnEA50BnVp9EKDvefjkySqPZydA/viewform"
    }
  }
}
