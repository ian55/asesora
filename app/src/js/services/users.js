import Service from '../infrastructure/service'

export default class Users extends Service {
  constructor() {
    super();
  }

  subscribe(){
    this.bus.subscribe("get.authorized-users", this.retrieveAuthorizedUsers.bind(this));
    this.bus.subscribe("get.authorized-coordiantors", this.retrieveAuthorizedCoordinators.bind(this));
    this.bus.subscribe("add.authorized-email", this.addEmail.bind(this));
    this.bus.subscribe("add.coordiantor-email", this.addCoordinatorEmail.bind(this));
    this.bus.subscribe("remove.authorized-user", this.removeAuthorizedUser.bind(this));
    this.bus.subscribe("remove.coordinator", this.removeCoordinator.bind(this));
  }

  retrieveAuthorizedUsers(){
    let callback = this.buildCallback('got.authorized-users')
    let body = {}
    let url = 'retrieve-authorized-users'
    this.client.hit(url, body, callback)
  }

  retrieveAuthorizedCoordinators() {
    let callback = this.buildCallback('got.authorized-coordinators')
    let body = {}
    let url = 'retrieve-coordinators'
    this.client.hit(url, body, callback)
  }

  addEmail(email){
    let callback = this.buildCallback('added.authorized-user')
    let body = email
    let url = 'add-authorized-user'
    this.client.hit(url, body, callback)
  }

  addCoordinatorEmail(email){
    let callback = this.buildCallback('added.authorized-coordinator')
    let body = email
    let url = 'add-coordinator'
    this.client.hit(url, body, callback)
  }

  removeAuthorizedUser(email) {
    let callback = this.buildCallback('removed.authorized-user')
    let body = email
    let url = 'remove-authorized-user'
    this.client.hit(url, body, callback)
  }

  removeCoordinator(email) {
    let callback = this.buildCallback('removed.authorized-coordinator')
    let body = email
    let url = 'remove-coordinator'
    this.client.hit(url, body, callback)
  }
}
