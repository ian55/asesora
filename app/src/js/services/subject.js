import { APIClient } from '../infrastructure/api_client'
import isEmpty from '../lib/isEmpty'
import {Bus} from '../bus'

export default channel => {

  const retrieveSubjects = user_id => {
    const callback = channelCallback('got.subjects-list')
    const url = 'retrieve-subjects-by-domain'
    APIClient.hit(url, { user_id }, callback)
  }

  const getSolicitude = id => {
    const url = 'retrieve-solicitude'
    const callback = channelCallback('got.solicitude')
    APIClient.hit(url, { id }, callback)
  }

  const retrieveTopics = () => {
    let url = 'topics'
    let callback = channelCallback('got.topics-catalog')
    APIClient.hit(url, {}, callback)
  }

  const retrieveProposals = () => {
    let url = 'proposals'
    let callback = channelCallback('got.proposals-catalog')
    APIClient.hit(url, {}, callback)
  }

  const retrieveProjects = () => {
    let url = 'projects'
    let callback = channelCallback('got.projects-catalog')
    APIClient.hit(url, {}, callback)
  }

  const retrieveReasons = () => {
    let url = 'close-reasons'
    let callback = channelCallback('got.reasons-catalog')
    APIClient.hit(url, {}, callback)
  }

  const createSubject = subject => {
    const url = 'create-subject'
    const callback = channelCallback('subject.created')
    APIClient.hit(url, subject, callback)
  }

  const updateSubject = payload => {
    let url = 'update-subject'
    let callback = channelCallback('subject.updated')
    APIClient.hit(url, payload, callback)
  }

  const closeSubject = payload => {
    let url = 'close-subject'
    let callback = channelCallback('subject.closed')
    APIClient.hit(url, payload, callback)
  }

  const deleteSubject = id => {
    let url = 'delete-subject'
    let callback = channelCallback('deleted.subject')
    APIClient.hit(url, { subjectId: id }, callback)
  }

  const channelCallback = signal => response => channel.publish(signal, response)

  return {
    start: () => {
      channel.subscribe("get.subjects-list", user_id => retrieveSubjects(user_id))
      channel.subscribe("get.solicitude", user_id => getSolicitude(user_id))

      channel.subscribe("create.subject", subject => createSubject(subject))
      channel.subscribe("update.subject", updateSubject.bind(this))
      channel.subscribe("close.subject", closeSubject.bind(this))
      channel.subscribe("delete.subject", deleteSubject.bind(this))

      retrieveTopics()
      retrieveProposals()
      retrieveProjects()
      retrieveReasons()
    }
  }
}
