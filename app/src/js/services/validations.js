import { Bus } from '../bus'
import isEmpty from '../lib/isEmpty'
import MaxEmployees from '../lib/MaxEmployees'

export default class Validations {
  constructor() {
    this.subscriptions()
  }

  subscriptions() {
    Bus.subscribe("check.subject.justified", this.isJustifiedSubject.bind(this))
    Bus.subscribe("check.solicitude.justified", this.isJustifiedSolicitude.bind(this))
  }

  isJustifiedSubject(payload) {
    const warning = (
      isEmpty(payload.selectedTopics) ||
      isEmpty(payload.proposals)
    )

    Bus.publish('checked.subject.justified', warning)
  }

  isJustifiedSolicitude(payload){

    const warning =(
      isEmpty(payload.companyEmployees) ||
      isEmpty(payload.companyCnae) ||
      isEmpty(payload.source.text) ||
      isEmpty(payload.applicantCcaa.text) ||
      isEmpty(payload.date) ||
      this.hasTooManyEmployees(payload.companyEmployees)
    )
    Bus.publish('checked.solicitude.justified', warning)
  }

  hasTooManyEmployees(companyEmployees){
    return MaxEmployees.isOver(companyEmployees)
  }
}
