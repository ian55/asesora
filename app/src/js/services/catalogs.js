import { APIClient } from '../infrastructure/api_client'
import BuildCallback from '../infrastructure/buildCallback'
import { Bus } from '../bus'

class Catalogs {
  constructor() {
    this.buildCallback = BuildCallback
    this.client = APIClient

    this.askCatalogs()
    this.subscriptions()
  }

  askCatalogs() {
    this.retrieveCnae()
    this.retrieveTopics()
    this.retrieveProposals()
    this.retrieveProjects()
    this.retrieveReasons()
    this.retriveCatalogSourceFormats()
    this.retriveOrganizations()
    this.retrieveCcaa()
  }

  subscriptions() {
    Bus.subscribe('update.item.in.catalog', this.updateCatalog.bind(this))
    Bus.subscribe('refresh.catalogs', this.askCatalogs.bind(this))
    Bus.subscribe('add.item.to.catalog', this.addItemToCatalog.bind(this))
  }

  retrieveCcaa(){
    let callback = this.buildCallback('got.ccaa-catalog')
    let body = {}
    let url = 'ccaa'
    this.client.hit(url, body, callback)
  }

  retrieveReasons() {
    let callback = this.buildCallback('got.reasons-catalog')
    let body = {}
    let url = 'close-reasons'
    this.client.hit(url, body, callback)
  }

  retrieveCnae() {
    let callback = this.buildCallback('got.cnae-catalog')
    let body = {}
    let url = 'cnae'
    this.client.hit(url, body, callback)
  }

  retrieveProposals() {
    let callback = this.buildCallback('got.proposals-catalog')
    let body = {}
    let url = 'proposals'
    this.client.hit(url, body, callback)
  }

  retrieveProjects() {
    let callback = this.buildCallback('got.projects-catalog')
    let body = {}
    let url = 'projects'
    this.client.hit(url, body, callback)
  }

  retrieveTopics() {
    let callback = this.buildCallback('got.topics-catalog')
    let body = {}
    let url = 'topics'
    this.client.hit(url, body, callback)
  }

  retriveCatalogSourceFormats() {
    let callback = this.buildCallback('got.source-formats-catalog')
    let body = {}
    let url = 'source_formats'
    this.client.hit(url, body, callback)
  }

  retriveOrganizations() {
    let callback = this.buildCallback('got.organizations-catalog')
    let body = {}
    let url = 'organizations-catalog'
    this.client.hit(url, body, callback)
  }

  updateCatalog(payload) {
    const callback = this.buildCallback('updated.item.in.catalog')
    const body = payload
    const url = 'update-catalog'
    APIClient.hit(url, body, callback)
  }

  addItemToCatalog(payload){
    const callback = this.buildCallback('added.item.in.catalog')
    const body = payload
    const url = 'add-item-to-catalog'
    APIClient.hit(url, body, callback)
  }
}

export default Catalogs
