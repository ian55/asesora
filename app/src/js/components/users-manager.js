import Component from '../infrastructure/component'
import Service from '../services/users'
import Models from '../models/models'
import view from '../views/users-manager'
import { Bus } from '../bus'
import { askTranslationsFor } from "../infrastructure/translatable"
import { isValidEmail } from '../lib/validations'

class UsersManager extends Component{

  constructor(){
    new Service();
    super();
  }

  subscribe(){
    Bus.subscribe("users-manager.created", this.initComponent.bind(this))
    Bus.subscribe("users-manager.destroyed", this.clearSubscriptions.bind(this))
  }

  collectSubscriptions() {
    this.addSubscription("got.translation.for.users-manager", this.translate.bind(this));
    this.addSubscription("got.authorized-users", this.loadUsers.bind(this));
    this.addSubscription("got.authorized-coordinators", this.loadCoordinators.bind(this));
  }

  watchActions(){
    this.addSubscription("added.authorized-user", this.getAuthorizedUsers.bind(this))
    this.addSubscription("added.authorized-coordinator", this.getAuthorizedCoordinators.bind(this))
    this.addSubscription("removed.authorized-user", this.getAuthorizedUsers.bind(this))
    this.addSubscription("removed.authorized-coordinator", this.getAuthorizedCoordinators.bind(this))
    this.addSubscription("clicked.email.add.button", this.addEmail.bind(this))
    this.addSubscription("clicked.add.coordinator.email", this.addCoordinatorEmail.bind(this))
    this.addSubscription("changed.email", this.validateEmail.bind(this))
    this.addSubscription("changed.coordinator.email", this.validateCoordinatorEmail.bind(this))
    this.addSubscription("clicked.remove.user", this.removeUser.bind(this))
    this.addSubscription("clicked.remove.coordinator", this.removeCoordinator.bind(this))
  }

  initComponent(){
    this.registerSubscriptions()
    this.getAuthorizedUsers();
    this.getAuthorizedCoordinators();
    askTranslationsFor({ labels: this.data.labels, scope: "users-manager" })
  }

  getAuthorizedUsers(){
    Bus.publish("get.authorized-users");
  }

  getAuthorizedCoordinators() {
    Bus.publish("get.authorized-coordiantors");
  }

  validateEmail({ detail }){
    this.data.control.isValidEmail = isValidEmail(detail.value);
    this.data.control.hasError = !this.data.control.isValidEmail && detail.value.length
  }

  validateCoordinatorEmail({ detail }){
    this.data.control.isValidCoordinatorEmail = isValidEmail(detail.value);
    this.data.control.hasCoordinatorError = !this.data.control.isValidCoordinatorEmail && detail.value.length
  }

  loadUsers(payload) {
    const authorizedUsers = payload.data.content
    this.data.values.emails = authorizedUsers.reverse()
  }

  addEmail() {
    const email = this.data.values.emailNew;
    this.data.values.emailNew = "";
    Bus.publish("add.authorized-email", { email });
  }

  loadCoordinators(payload) {
    const authorizedCoordinators = payload.data.content
    this.data.values.coordinatorEmails = authorizedCoordinators.reverse()
  }

  addCoordinatorEmail() {
    const email = this.data.values.coordinatorEmailNew;
    this.data.values.coordinatorEmailNew = "";
    Bus.publish("add.coordiantor-email", { email });
  }

  removeUser(event) {
    const email = event.detail
    Bus.publish("remove.authorized-user", { email });
  }

  removeCoordinator(event) {
    const email = event.detail
    if (this.data.values.coordinatorEmails.includes(email)) {
      Bus.publish("remove.coordinator", { email });
    }
  }

  model() {
    if(!this.data) { this.data = Models.newUsersManager() }

    return this.data
  }

  view() {
    return view
  }

}

const component = new UsersManager()

export default Object.assign({}, component.view, { data: component.model })
