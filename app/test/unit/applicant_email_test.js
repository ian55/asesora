import { mount } from '@vue/test-utils'
import ApplicantEmail from '../../src/js/views/solicitude/applicant/applicant-email.vue';

describe('Applicant email', () => {

  it('shows error when no valid email', () => {
    const props = {
      isValidEmail: false,
      values: {
        applicantEmail: 'valid.email@domain.com'
      },
      labels: {
        errorEmail: 'error text'
      }
    }

    const wrapper = mount(ApplicantEmail, {
      propsData: props
    })

    expect(wrapper.find("#contact-email").exists()).to.be.true;
  })
})
