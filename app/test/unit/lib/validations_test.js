import CheckOwnerInfoAvailable from '../../../src/js/lib/checkOwnerAvailable'

describe('is Show Owner', () => {
  it('you do not have to show if the owner is the one who is logged in', ()=> {
    const data = {
      owner: "owner@owner.com",
      logged: "owner@owner.com",
      isCoordinator: false
    }

    const result = CheckOwnerInfoAvailable.do(data)

    expect(result).to.be.false
  })

  it ('if it is from the domain of istas the owner has to be shown  ', ()=> {
    const data = {
      owner: "owner@owner.com",
      logged: "domain@istas.ccoo.es",
      isCoordinator: false
    }

    const result = CheckOwnerInfoAvailable.do(data)

    expect(result).to.be.true
  })

  it ('if it is a coordinator and belongs to the owners domain, it must be shown', ()=> {
    const data = {
      owner: "owner@owner.com",
      logged: "logged@owner.com",
      isCoordinator: true
    }

    const result = CheckOwnerInfoAvailable.do(data)

    expect(result).to.be.true
  })

  it ('if it is a coordinator and does not belong to the owners domain, it must be shown', ()=> {
    const data = {
      owner: "owner@owner.com",
      logged: "logged@other.com",
      isCoordinator: true
    }

    const result = CheckOwnerInfoAvailable.do(data)

    expect(result).to.be.false
  })

})
