import { SubjectsList } from "../../../src/js/components/subjects-list"
import * as converter from "../../../src/js/infrastructure/json_to_excel";

describe("Subjects-list component", () => {
  it("can download user's counselings CSV", () => {
    const subjectsList = new SubjectsList()
    const sut = { data : [
      {
        solicitude_date: "2019-03-14",
        solicitude_text: "Solicitude text",
        solicitude_source: { value: "03", text: "Carta" },
        solicitude_applicant: "d918540d-318a-4c3e-bc29-49d8b48a083b",
        solicitude_creation_moment: "1552552402760",
        solicitude_edition_moment: 1552557760,
        solicitude_company: "12345678Z",
        solicitude_user: {
          user_id: "mgquiros@istas.ccoo.es",
          domain: "Domain"
        },
        applicant_name: "Applicant",
        applicant_surname: "Surname",
        applicant_email: "email@gmail.com",
        applicant_phonenumber: "981551816",
        applicant_ccaa: { value: "03", text: "Asturias, Principado de" },
        applicant_id: "d918540d-318a-4c3e-bc29-49d8b48a083b",
        company_name: "Empresa 1",
        company_cif: "12345678Z",
        company_employees: "23",
        company_cnae: "012 - Cultivos perennes",
        subject_proposal: [
          "Mejorar formación plantilla",
          "Cumplimiento derechos normativa"
        ],
        subject_description: "descripcion",
        subject_analysis: "analysis",
        subject_topics: [
          { id: "01.02", name: "Agentes físicos" },
          { id: "01.05", name: "Factores psicosociales" }
        ],
        subject_solicitude_id: "1552552402760",
        subject_reason: null,
        subject_comments: null,
        subject_closed: null,
        subject_id: "1552552432039",
        subject_project: {text: "Proyecto 1"}
      }
    ]}

    const jsonToExcelStub = sinon.stub(converter, "jsonToExcel").returns(sut.data)
    const counselings = subjectsList.downloadCounselings(sut)

    expect(jsonToExcelStub).to.be.calledOnce
    expect(counselings.length).to.be.eq(1)

    converter.jsonToExcel.restore()
  });
});
