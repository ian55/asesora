import { mount } from '@vue/test-utils'
import UserInfoView from '../../src/js/views/user-info.vue'

describe('User\'s info', () => {
  it('shows nothing when user info not present', function() {
    const user_name = ''
    const user_image_url = ''

    const wrapper = mount(UserInfoView, {
      propsData: {
        values: {
          "name": user_name,
          "imageUrl": user_image_url
        }
      }
    })

    expect(wrapper.find("#user-info").exists()).to.eq(false)
  })
  
  it('shows user info when pressent', function() {
    const user_name = 'Anonymous user'
    const user_image_url = 'image_url'

    const wrapper = mount(UserInfoView, {
      propsData: {
        values: {
          "name": user_name,
          "imageUrl": user_image_url
        }
      }
    })

    expect(wrapper.find("#user-name").text()).to.eq(user_name)
    expect(wrapper.find("#user-imageUrl").attributes().src).to.eq(user_image_url)
  })

})
