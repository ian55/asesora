require_relative '../system/actions/authorize_email'
require_relative '../system/actions/check_authorization'
require_relative '../system/actions/retrieve_authorized_users'
require_relative '../system/actions/retrieve_coordinators'

module Endpoints
  class Authorization
    def self.define_endpoints(api)
      api.post '/api/authorization' do
        payload = JSON.parse(request.body.read)
        message = Actions::AuthorizeEmail.do(payload)
        message.to_json
      end

      api.post '/api/check-authorization' do
        payload = JSON.parse(request.body.read)
        message = Actions::CheckAuthorization.do(payload)
        message.to_json
      end

      api.post '/api/retrieve-authorized-users' do
        message = Actions::RetrieveAuthorizedUsers.do()
        {data: message}.to_json
      end

      api.post '/api/add-authorized-user' do
        payload = JSON.parse(request.body.read)
        message = ::Catalogs::Service::add_authorized_user payload["email"]
        {data: message}.to_json
      end

      api.post '/api/remove-authorized-user' do
        payload = JSON.parse(request.body.read)
        message = ::Catalogs::Service::remove_authorized_user payload["email"]
        {data: message}.to_json
      end

      api.post '/api/retrieve-coordinators' do
        message = Actions::RetrieveCoordinators.do()
        {data: message}.to_json
      end

      api.post '/api/add-coordinator' do
        payload = JSON.parse(request.body.read)
        message = ::Catalogs::Service::add_coordinator payload["email"]
        {data: message}.to_json
      end

      api.post '/api/remove-coordinator' do
        payload = JSON.parse(request.body.read)
        message = ::Catalogs::Service::remove_coordinator payload["email"]
        {data: message}.to_json
      end

      api.post '/api/is-user-coordinator' do
        payload = JSON.parse(request.body.read)
        is_coordinator = ::Catalogs::Service::user_coordinator?(payload['user_id'])
        {data: is_coordinator}.to_json
      end
    end
  end
end
