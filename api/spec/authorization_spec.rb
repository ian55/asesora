require 'json'
require 'rack/test'
require 'fileutils'

require_relative '../asesora'
require_relative '../system/services/catalogs/service'

describe 'Authorization' do

  include Rack::Test::Methods

  def app
    Asesora
  end

  context 'on ask for user authorization' do
    it "retrieves an error when given not authorized email" do
      post '/api/authorization', {email: 'not_authorized_email@gmail.com'}.to_json
      response = JSON.parse(last_response.body)

      expect(response['error']).not_to be_empty
    end

    it "retrieves a token when given authorized email" do
      post '/api/authorization', {email: 'authorized_email@gmail.com'}.to_json
      response = JSON.parse(last_response.body)

      expect(response['error']).to be_empty
      expect(response['token']).not_to be_empty
    end

    it "retrieves a token when given another authorized email" do
      post '/api/authorization', {email: 'another_authorized_email@gmail.com'}.to_json
      response = JSON.parse(last_response.body)

      expect(response['error']).to be_empty
      expect(response['token']).not_to be_empty
    end
  end

  context 'on check' do
    before :each do
      ENV["SCHEMA"] = ""
    end

    it "retrieves an error when given expired token" do
      expired_time = Time.now.to_i - 4 * 60
      payload = {
        email: 'authorized_email@gmail.com',
        exp: expired_time
      }
      post '/api/authorization', payload.to_json
      response = JSON.parse(last_response.body)

      post '/api/check-authorization', { token: response["token"] }.to_json
      response = JSON.parse(last_response.body)

      expect(response['error']).not_to be_empty
    end

    it "retrieves an error when given invalid token" do
      post '/api/check-authorization', { token: nil }.to_json
      response = JSON.parse(last_response.body)

      expect(response['error']).not_to be_empty
    end

    it "renews token when given not expired one" do
      not_expired_time = Time.now.to_i + (4 * 60)
      payload = {
        email: 'authorized_email@gmail.com',
        exp: not_expired_time
      }

      post '/api/authorization', payload.to_json
      response = JSON.parse(last_response.body)
      old_token = response['token']

      post '/api/check-authorization', { token: old_token }.to_json
      response = JSON.parse(last_response.body)
      new_token = response['token']

      expect(new_token).not_to be_empty
      expect(old_token).not_to eq(new_token)
    end

    it "returns a token on development schema" do
      ENV['SCHEMA'] = "development"

      post '/api/check-authorization', { token: nil }.to_json
      response = JSON.parse(last_response.body)

      expect(response['token']).not_to be_empty
    end

    it 'returns if user is coordinator' do
      post '/api/is-user-coordinator', { user_id: "coordinator@gmail.com"}.to_json
      response = JSON.parse(last_response.body)

      expect(response['data']).to be true
    end

    it 'returns if user is not coordinator' do
      post '/api/is-user-coordinator', { user_id: "notCoordinator@gmail.com"}.to_json
      response = JSON.parse(last_response.body)

      expect(response['data']).to be false
    end
  end

  context 'coordinators' do
    it 'can add to coordinators list' do
      sample_email = "sample@email.com"
      post '/api/add-coordinator', { email: sample_email}.to_json

      post '/api/retrieve-coordinators'
      response = JSON.parse(last_response.body)

      expect(response['data']['content']).to include(sample_email)
    end

    it 'can be removed from coordinators list' do
      sample_email = "sample@email.com"
      post '/api/add-coordinator', { email: sample_email}.to_json
      post '/api/retrieve-coordinators'
      response = JSON.parse(last_response.body)
      expect(response['data']['content']).to include(sample_email)

      post '/api/remove-coordinator', { email: sample_email}.to_json

      post '/api/retrieve-coordinators'
      response = JSON.parse(last_response.body)
      expect(response['data']['content']).not_to include(sample_email)
    end
  end
end
