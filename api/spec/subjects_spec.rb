require 'rspec'
require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'

describe 'Subjects Api' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  before(:each) do
    post 'fixtures/clean'
  end

  after(:all) do
    post 'fixtures/clean'
    Fixtures.new.drop_sftp
  end

	it 'creates subjects' do
    solicitude = create_solicitude

		subject = create_subject(solicitude['creation_moment'])

    expect(subject["proposal"]).to eq(Fixtures::SUBJECT_PROPOSAL)
    expect(subject["solicitude_id"]).to eq(solicitude['creation_moment'])
  end

  it 'retrieves subjects by solicitude with attachments' do
    Fixtures.new.drop_sftp
    solicitude = create_solicitude
    subject = create_subject_with_attachment(solicitude['creation_moment'])
    data = { "solicitudeId": solicitude['creation_moment'] }

    post '/api/retrieve-subjects', data.to_json

    subjects = JSON.parse(last_response.body)['data']
    expect(subjects[0]["proposal"]).to eq(Fixtures::SUBJECT_PROPOSAL)
    expect(subjects[0]["description"]).to eq(Fixtures::SUBJECT_PROPOSAL_DESCRIPTION)
    expect(subjects[0]["solicitude_id"]).to eq(solicitude['creation_moment'])
    expect(subjects[0]["files"][0]["name"]).to eq("attach")
	end

  it 'retrieves subjects by solicitude without attachments' do
    Fixtures.new.drop_sftp
    solicitude = create_solicitude
    subject = create_subject(solicitude['creation_moment'])
    data = { "solicitudeId": solicitude['creation_moment'] }

    post '/api/retrieve-subjects', data.to_json

    subjects = JSON.parse(last_response.body)['data']
    expect(subjects[0]["proposal"]).to eq(Fixtures::SUBJECT_PROPOSAL)
    expect(subjects[0]["description"]).to eq(Fixtures::SUBJECT_PROPOSAL_DESCRIPTION)
    expect(subjects[0]["solicitude_id"]).to eq(solicitude['creation_moment'])
  end

  it 'has incremental numeration' do
    solicitude = create_solicitude('user@domain.com')

    subject = create_subject(solicitude['creation_moment'])
    another_subject = create_subject(solicitude['creation_moment'])

    expect(subject['numeration']).to eq('domain.com-1')
    expect(another_subject['numeration']).to eq('domain.com-2')
  end

  it 'updates subject' do
    solicitude = create_solicitude
    subject = create_subject(solicitude['creation_moment'])
    updated_values = {
      "subjectId": subject['id'],
      "proposal": Fixtures::SUBJECT_PROPOSAL_2,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION_2,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS_2,
      "topics": Fixtures::SUBJECT_TOPICS,
      "numeration": Fixtures::SUBJECT_NUMERATION
		}

    post '/api/update-subject', updated_values.to_json

    subject = JSON.parse(last_response.body)
    expect(subject["proposal"]).to eq(Fixtures::SUBJECT_PROPOSAL_2)
    expect(subject["description"]).to eq(Fixtures::SUBJECT_PROPOSAL_DESCRIPTION_2)
    expect(subject["analysis"]).to eq(Fixtures::SUBJECT_PROPOSAL_ANALYSIS_2)
    expect(subject["topics"]).to eq(Fixtures::SUBJECT_TOPICS)
  end

  it 'can close a subject' do
    solicitude = create_solicitude
    subject = create_subject(solicitude['creation_moment'])
    expect(subject["closed"]).to be_nil

    subject = close_subject(subject['id'])

    expect(subject["reason"]).to eq("A reason")
    expect(subject["comments"]).to eq("A comment")
    expect(subject["closed"]).not_to be_nil
  end

  it 'can create closed subjects' do
    solicitude = create_solicitude

    closed_subject = create_closed_subject(solicitude['creation_moment'])

    expect(closed_subject["id"]).not_to eq(nil)
    expect(closed_subject["reason"]).to eq("A reason")
    expect(closed_subject["comments"]).to eq("A comment")
    expect(closed_subject["closed"]).not_to eq(nil)
  end

  it 'deletes subject' do
    solicitude = create_solicitude
    subject = create_subject(solicitude['creation_moment'])
    subjects_before = retrieve_subjects(solicitude['creation_moment']).count
    subject_to_delete = { "subjectId": subject['id'] }

    post '/api/delete-subject', subject_to_delete.to_json

    subjects_after = retrieve_subjects(solicitude['creation_moment']).count
    is_deleted = subjects_before > subjects_after
    expect(is_deleted).to be(true)
  end

  it 'delete subject return error 500 when subject not exist ' do
    subject_inexitent = "0"
    subject_for_delete = {
      "solicitudeId": Fixtures::CREATION_MOMENT,
      "subjectId": subject_inexitent
    }.to_json

    post '/api/delete-subject', subject_for_delete
    response = last_response.status

    expect(response).to eq(500)
  end

  context 'retrieves subjects by user getting' do
    it 'one that contains solicitude and subject ids' do
      solicitude_id = create_solicitude()['creation_moment']
      subject_id = create_subject(solicitude_id)["id"]
      subjects = retrieve_user_subjects

      expect(subjects[0]['subject_id']).to eq(subject_id)
      expect(subjects[0]['solicitude_id']).to eq(solicitude_id)
    end

    it 'one from different users' do
      user_id = "user_one@gmail.com"
      first_solicitude_id = create_solicitude_with_subject(user_id)["solicitude_id"]

      another_user_id = "user_two@gmail.com"
      second_solicitude_id = create_solicitude_with_subject(another_user_id)['solicitude_id']

      subjects = retrieve_user_subjects(user_id)

      expect(subjects[0]['solicitude_id']).to eq(first_solicitude_id)
    end

    it 'two from the same user' do
      first_solicitude_id = create_solicitude_with_subject["solicitude_id"]
      second_solicitude_id = create_solicitude_with_subject["solicitude_id"]
      subjects = retrieve_user_subjects

      expect(subjects[1]['solicitude_id']).to eq(first_solicitude_id)
      expect(subjects[0]['solicitude_id']).to eq(second_solicitude_id)
    end
  end

  it "retrieves user's counselings" do
    solicitude_id = create_solicitude()['creation_moment']
    subject_id = create_subject(solicitude_id)["id"]
    close_subject(subject_id)
    create_solicitude_with_subject

    body = {
      user_id: Fixtures::USER_ID
    }.to_json

    post 'api/retrieve-subjects-by-user-csv', body
    counselings = JSON.parse(last_response.body)["data"]

    counseling = counselings[0]

    expect(counselings.count).to eq(2)
    expect(counseling["solicitude_date"]).to eq(Fixtures::DATE)
    expect(counseling["solicitude_text"]).to eq(Fixtures::TEXT)
    expect(counseling["solicitude_source"]).to eq(Fixtures::SOURCE)
    expect(counseling["applicant_name"]).to eq(Fixtures::APPLICANT_NAME)
    expect(counseling["applicant_surname"]).to eq(Fixtures::APPLICANT_SURNAME)
    expect(counseling["applicant_email"]).to eq(Fixtures::APPLICANT_EMAIL)
    expect(counseling["applicant_phonenumber"]).to eq(Fixtures::APPLICANT_PHONENUMBER)
    expect(counseling["applicant_ccaa"]).to eq(Fixtures::APPLICANT_CCAA)
    expect(counseling["company_name"]).to eq(Fixtures::COMPANY_NAME)
    expect(counseling["company_cif"]).to eq(Fixtures::COMPANY_CIF)
    expect(counseling["company_employees"]).to eq(Fixtures::COMPANY_EMPLOYEES)
    expect(counseling["company_cnae"]).to eq(Fixtures::COMPANY_CNAE)
    expect(counseling["subject_topics"]).to eq(Fixtures::SUBJECT_TOPICS)
    expect(counseling["subject_analysis"]).to eq(Fixtures::SUBJECT_PROPOSAL_ANALYSIS)
    expect(counseling["subject_proposal"]).to eq(Fixtures::SUBJECT_PROPOSAL)
    expect(counseling["subject_description"]).to eq(Fixtures::SUBJECT_PROPOSAL_DESCRIPTION)
  end

  it 'removes closed status of conseling' do
    solicitude = create_solicitude
    subject = create_subject(solicitude['creation_moment'])
    closed_subject = close_subject(subject['id'])
    body = { id: closed_subject["id"] }

    post 'api/remove-closed-status', body.to_json

    response = JSON.parse(last_response.body)
    expect(response["id"]).not_to be_nil
    expect(response["reason"]).to be_nil
    expect(response["comments"]).to be_nil
    expect(response["closed"]).to be_nil
  end

  it "retrieves organization's subjects" do
    user = 'user@asesora.com'
    solicitude = create_solicitude(user)
    create_subject(solicitude['creation_moment'])
    create_subject(solicitude['creation_moment'])
    another_solicitude = create_solicitude(Fixtures::ANOTHER_USER_ID)
    create_subject(another_solicitude['creation_moment'])
    body = { 'user_id' => user }

    post '/api/retrieve-subjects-by-domain', body.to_json

    response = JSON.parse(last_response.body)
    subject = response['data'].first
    expect(subject['user_id']).to eq(user)
    expect(subject['date']).to eq(solicitude['date'])
    expect(subject['solicitude_numeration']).to eq(solicitude['numeration'])
    expect(response['data'].size).to eq(2)
  end

  it "retrieves all subjects" do
    first_user = 'user@asesora.com'
    second_user = 'user@istas.com'

    first_solicitude = create_solicitude(first_user)
    create_subject(first_solicitude['creation_moment'])

    second_solicitude = create_solicitude(second_user)
    create_subject(second_solicitude['creation_moment'])

    body = { 'user_id' => first_user }

    post '/api/retrieve-all-subjects', body.to_json

    response = JSON.parse(last_response.body)

    expect(response['data'].size).to eq(2)
  end

  it "retrieves organization's subjects less mine" do
    user = Fixtures::ANOTHER_USER_ID
    another_user = 'second' + Fixtures::ANOTHER_USER_ID
    solicitude = create_solicitude(user)
    create_subject(solicitude['creation_moment'])
    another_solicitude = create_solicitude(another_user)
    create_subject(another_solicitude['creation_moment'])
    create_subject(another_solicitude['creation_moment'])
    body = { 'user_id' => user }

    post '/api/retrieve-subjects-by-domain-less', body.to_json

    response = JSON.parse(last_response.body)
    subject = response['data'].first
    expect(subject['user_id']).to eq(another_user)
    expect(response['data'].size).to eq(2)
  end

  it "retrieves other organizations subjects" do
    another_user = 'user@ugt.org'
    solicitude = create_solicitude(another_user)
    create_subject(solicitude['creation_moment'])
    user = Fixtures::ANOTHER_USER_ID
    solicitude = create_solicitude(user)
    create_subject(solicitude['creation_moment'])
    body = { 'user_id' => user }

    post '/api/retrieve-subjects-less-my-domain', body.to_json

    response = JSON.parse(last_response.body)
    expect(response['data'].first['user_id']).to eq(another_user)
    expect(response['data'].size).to eq(1)
  end

  private

  def create_solicitude(user_id = Fixtures::USER_ID, domain = Fixtures::DOMAIN)
    body = {
      'applicantName': Fixtures::APPLICANT_NAME,
      'applicantSurname': Fixtures::APPLICANT_SURNAME,
      'applicantEmail': Fixtures::APPLICANT_EMAIL,
      'applicantPhonenumber': Fixtures::APPLICANT_PHONENUMBER,
      'applicantCcaa': Fixtures::APPLICANT_CCAA,
      'text': Fixtures::TEXT,
      'date': Fixtures::DATE,
      'source': Fixtures::SOURCE,
      'applicantId': "",
      'companyCif': Fixtures::COMPANY_CIF,
      'companyName': Fixtures::COMPANY_NAME,
      'companyCnae': Fixtures::COMPANY_CNAE,
      'companyEmployees': Fixtures::COMPANY_EMPLOYEES,
      'user_id': user_id,
      'domain': domain
    }

    post_create_solicitude(body.to_json)

    JSON.parse(last_response.body)
  end

  def create_subject(solicitude_id = Fixtures::CREATION_MOMENT)
    body = {
      "solicitudeId": solicitude_id,
      "topics": Fixtures::SUBJECT_TOPICS,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS,
      "proposal": Fixtures::SUBJECT_PROPOSAL,
      "project": Fixtures::PROJECT,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION
    }
    post '/api/create-subject', body.to_json

    JSON.parse(last_response.body)
  end

  def create_subject_with_attachment(solicitude_id = Fixtures::CREATION_MOMENT)
    body = {
      "solicitudeId": solicitude_id,
      "topics": Fixtures::SUBJECT_TOPICS,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS,
      "proposal": Fixtures::SUBJECT_PROPOSAL,
      "project": Fixtures::PROJECT,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION,
      "files": Fixtures::FILES
    }
    post '/api/create-subject', body.to_json

    JSON.parse(last_response.body)
  end

  def create_solicitude_with_subject(user_id = Fixtures::USER_ID, domain = Fixtures::DOMAIN)
    solicitude_id = create_solicitude(user_id, domain)['creation_moment']
    subject_id = create_subject(solicitude_id)['id']

    {
      "solicitude_id" => solicitude_id,
      "subject_id" => subject_id
    }
  end

  def create_closed_subject(solicitude)
    body = {
      "solicitudeId": solicitude,
      "topics": Fixtures::SUBJECT_TOPICS,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS,
      "proposal": Fixtures::SUBJECT_PROPOSAL,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION,
      "reason": Fixtures::SUBJECT_CLOSE_REASON,
      "comments": Fixtures::SUBJECT_CLOSE_COMMENTS,
      "subjectId": ''
    }

    post '/api/close-subject', body.to_json

    JSON.parse(last_response.body)
  end

  def close_subject(subject_id = "")
    body = {
      "solicitudeId": Fixtures::CREATION_MOMENT,
      "topics": Fixtures::SUBJECT_TOPICS,
      "analysis": Fixtures::SUBJECT_PROPOSAL_ANALYSIS,
      "proposal": Fixtures::SUBJECT_PROPOSAL,
      "description": Fixtures::SUBJECT_PROPOSAL_DESCRIPTION,
      "subjectId": subject_id,
      "reason": Fixtures::SUBJECT_CLOSE_REASON,
      "comments": Fixtures::SUBJECT_CLOSE_COMMENTS,
      "numeration": Fixtures::SUBJECT_NUMERATION
		}.to_json

    post '/api/close-subject', body

    JSON.parse(last_response.body)
  end

  def retrieve_user_subjects(user_id = Fixtures::USER_ID)
    user = {
      "user_id": user_id,
      "domain": Fixtures::DOMAIN
    }.to_json

    post '/api/retrieve-subjects-by-user', user

    JSON.parse(last_response.body)["data"]
  end

  def retrieve_subjects(solicitude_id = Fixtures::CREATION_MOMENT)
    body = {
      "solicitudeId": solicitude_id
    }.to_json

    post '/api/retrieve-subjects', body

    JSON.parse(last_response.body)["data"]
  end

  def retrieve_domain_subjects(domain = Fixtures::DOMAIN)
    user = {
      "domain": domain
    }.to_json

    post '/api/retrieve-subjects-by-domain', user

    JSON.parse(last_response.body)["data"]
  end

  def post_create_solicitude(body_created)
    post '/api/create-solicitude', body_created
  end

end
