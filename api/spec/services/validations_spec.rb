require_relative  '../../system/services/validations/service'


describe 'validation' do
  context 'knows show owner' do

    it "not if the owner is logged in" do
      owner = "owner@owner.com"
      logged = "owner@owner.com"
      isCoordinator = false

      result = ::Validations::Service.show_owner?(owner, logged, isCoordinator)

      expect(result).to be false
    end

    it "if it is from the domain of istas" do
      owner = "owner@owner.com"
      logged = "domain@istas.ccoo.es"
      isCoordinator = false

      result = ::Validations::Service.show_owner?(owner, logged, isCoordinator)

      expect(result).to be true

    end

    it "if logged user is a coordinator of counseling domain" do
      owner = "owner@owner.com"
      logged = "logged@owner.com"
      isCoordinator = true

      result = ::Validations::Service.show_owner?(owner, logged, isCoordinator)

      expect(result).to be true
    end

    it "not if is coordinator but from other domain" do
      owner = "owner@owner.com"
      logged = "logged@other.com"
      isCoordinator = true

      result = ::Validations::Service.show_owner?(owner, logged, isCoordinator)

      expect(result).to be false
    end
  end
end
