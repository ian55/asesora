require 'json'
require 'rack/test'

require_relative './fixtures/asesora_with_fixtures'
require_relative './fixtures/fixtures'

describe 'Users' do

  include Rack::Test::Methods

  def app
    AsesoraWithFixtures
  end

  before(:each) do
    Fixtures.new.reset_authorized_users_catalog
  end

  after(:each) do
    Fixtures.new.reset_authorized_users_catalog
  end

  it 'returns complete authorized users catalog' do

    post '/api/retrieve-authorized-users', {}
    catalog = JSON.parse(last_response.body)

    expect(catalog['data']['name']).to eq('authorized_users')
    expect(catalog['data']['content'].size).to eq(5)
  end

  it 'can add one authorized user' do
    new_user = "new_user@gmail.com"

    post '/api/add-authorized-user', {email: new_user}.to_json
    response = post '/api/retrieve-authorized-users'
    authorized_users_updated = JSON.parse(response.body)["data"]

    expect(authorized_users_updated['content']).to include(new_user)
  end

  it 'can add two authorized users' do
    new_user_one = "new_user_one@gmail.com"
    post '/api/add-authorized-user', {email: new_user_one}.to_json

    new_user_two = "new_user_two@gmail.com"
    post '/api/add-authorized-user', {email: new_user_two}.to_json

    response = post '/api/retrieve-authorized-users'
    authorized_users = JSON.parse(response.body)["data"]

    expect(authorized_users['content']).to include(new_user_one, new_user_two)
  end

  it 'can remove a user from authorization collection' do
    new_user = "new_user@gmail.com"

    post '/api/add-authorized-user', {email: new_user}.to_json
    post '/api/remove-authorized-user', {email: new_user}.to_json
    response = post '/api/retrieve-authorized-users'
    authorized_users_updated = JSON.parse(response.body)["data"]

    expect(authorized_users_updated).not_to include(new_user)
  end
end
