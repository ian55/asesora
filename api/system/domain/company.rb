require 'securerandom'

module Domain
  class NullCompany
    def serialize
      return {}
    end

    def nil?
      true
    end
  end

  class Company
    def self.from_document(document)
      company = new(document['id'])
      company.cif = document['cif']
      company.name = document['name']
      company.employees = document['employees']
      company.cnae = document['cnae']
      company.cp = document['cp']

      company
    end

    def self.with(name, cif, employees, cnae, cp, id=nil)
      company = new(id)
      company.cif = cif
      company.name = name
      company.employees = employees
      company.cnae = cnae
      company.cp = cp

      company
    end

    def self.nullified
      return NullCompany.new
    end

    def initialize(id)
      @id = generate(id)
      @state = {}
    end

    def memento
      memento = @state.clone
      memento[:signature] = @id
      memento[:timestamp] = Time.now.to_i
      memento
    end

    def remind(memento)
      return unless memento[:signature] == @id
      state_candidate = memento.clone
      state_candidate.delete(:signature)
      state_candidate.delete(:timestamp)
      @state = state_candidate
    end

    private_class_method :new

    def identify
      @id
    end

    def name=(a_name)
      @name = a_name
      @state[:name] = a_name
    end

    def employees=(a_employees)
      @employees = a_employees
      @state[:employees] = a_employees
    end

    def cnae=(a_cnae)
      @cnae = a_cnae
    end

    def cif=(cif)
      @cif = cif
    end

    def cp=(cp)
      @cp = cp
      @state[:cp] = cp
    end

    def nil?
      false
    end

    def serialize
      {
        "name" => @state[:name],
        "cif" => @cif,
        "cp" => @cp,
        "employees" => @state[:employees],
        "cnae" => @cnae,
        "id" => @id
      }
    end

    private

    def generate(id)
      return id  unless id.nil? || id.empty?

      SecureRandom.uuid
    end
  end
end
