require_relative '../services/authorization/service'

module Actions
  class CheckAuthorization
    def self.do(payload)
      if in_development?
        payload = factory_payload()
      end
      error = ''
      token = ::Authorization::Service.check_token(payload)

      if token == ""
        error = 'invalid token'
      end

      { error: error, token: token }
    end

    def self.factory_payload
      expiration_minutes = 4
      expiration_seconds = Time.now.to_i + (expiration_minutes * 60)
      authorized_email = 'authorized_email@gmail.com'

      return {
        "email" => authorized_email,
        "exp" => expiration_seconds,
        "token" => ::Authorization::Service.generate_token_for({
          "email" => authorized_email,
          "exp" => expiration_seconds
        })
      }
    end

    def self.in_development?
      ENV['SCHEMA'] == "development"
    end

  end
end
