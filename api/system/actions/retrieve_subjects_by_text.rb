require_relative '../services/subjects/service'

module Actions
  class RetrieveSubjectsByText
    def self.do(text)
      subjects = Subjects::Service.filter_by(text)
      subjects
    end
  end
end
