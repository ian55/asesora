require_relative '../services/attachments/service'

module Actions
  class DownloadAttachment
    class << self
      def do(id)
        downloaded = Attachments::Service.download(id)

        downloaded
      end
    end
  end
end
