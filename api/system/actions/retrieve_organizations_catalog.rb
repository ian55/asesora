require_relative '../services/catalogs/service'

module Actions
  class RetrieveOrganizationsCatalog
    def self.do(*_)
      ::Catalogs::Service.organizations
    end
  end
end
