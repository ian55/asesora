require_relative '../services/attachments/service'

module Actions
  class DeleteAttachment
    class << self
      def do(id:)
        Attachments::Service.delete(id)
      end
    end
  end
end
