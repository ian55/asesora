
class Email
  class << self
    def extract_domain(email)
      domain = email.split('@').last

      domain || ''
    end
  end
end
