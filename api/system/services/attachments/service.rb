require_relative '../../domain/attachment'
require_relative 'collection'

module Attachments
  class Service
    def self.create(file, subject_id)
      attachment = Domain::Attachment.with(file['name'], subject_id)
      Collection.create(attachment, file).serialize
    end

    def self.retrieve(subject_id)
      attachments = Collection.retrieve(subject_id)
      attachments.serialize
    end

    def self.delete(id)
      attachment = Collection.find_by_id(id)
      Collection.delete(attachment)

      attachment.serialize
    end

    def self.delete_for(subject)
      Collection.delete_for(subject)
    end

    def self.download(id)
      attachment = Collection.download(id)

      attachment.serialize
    end
  end
end
