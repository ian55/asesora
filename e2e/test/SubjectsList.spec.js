import Subjects from './pages/Subjects'
import Fixtures from './Fixtures'

describe('Subjects List', () => {
  const fixtures = new Fixtures()

  beforeEach(() => {
    Fixtures.pristine()
  })

  after(() => {
    Fixtures.clean()
  })

  xit('can list my organizations subjects less mine', () => {
    const subjects = new Subjects().filterByMyOrganizationLessMine()

    expect(subjects.countsSubjects(fixtures.myOrganizationSubjectsLessMine)).to.eq.true
  })

  xit('can list my subjects', () => {
    const subjects = new Subjects()

    expect(subjects.countsSubjects(fixtures.mySubjects)).to.eq.true
  })

  xit('can list my organizations subjects with mine', () => {
    const subjects = new Subjects().filterByMyOrganization()

    expect(subjects.countsSubjects(fixtures.myOrganizationSubjects)).to.eq.true
  })

  xit('can list other organizations subjects', () => {
    const subjects = new Subjects().filterByOtherOrganizations()

    expect(subjects.countsSubjects(fixtures.otherOrganizationsSubjects)).to.eq.true
  })
})
