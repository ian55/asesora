import UserManager from './pages/UserManager'

describe('Users Manager', () => {
  const timestamp = new Date().getTime()
  const email = timestamp + "@test.com"

  it('can add a user', () => {

    const userManager = new UserManager()
      .fillEmail(email)
      .add()

    expect(userManager.includes(email)).to.be.true
  })

  it('can remove a user', () => {

    const userManager = new UserManager().removeFirstUser()

    expect(userManager.listNotIncludes(email)).to.be.true
  })
})
